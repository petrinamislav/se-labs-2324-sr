# Input the comma-separated integers and transform them into a list of integers.
# Print the number of even ints in the given list. Note: the % "mod" operator computes the remainder, e.g. 5 % 2 is 1.
# Use a function for counting evens.

ulaz = input("Unesite brojeve (odvojene zarezom): ")
ulaz = ulaz.split(",")

num = []
print(ulaz)
for x in ulaz:
    num.append(int(x))

def count_evens(numbers):
    evens = 0
    for n in numbers:
        if n % 2 == 0:
            evens += 1
    
    return evens

print(count_evens(num))