# Input the comma-separated integers and transform them into a list of integers.
# Given a list of ints, return True if the list contains a 2 next to a 2 somewhere.

ulaz = input("Unesite brojeve (odvojene zarezom): ")
ulaz = ulaz.split(",")

num = []
print(ulaz)

for x in ulaz:
    num.append(int(x))

def has22(numb):
    length = len(numb)
    for x in numb:
        if x == length:
            return False
        if numb[x] == 2 and numb[x + 1] == 2:
            return True
    
    return False

print(has22(num))