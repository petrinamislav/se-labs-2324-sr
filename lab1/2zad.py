# Input the comma-separated integers and transform them into a list of integers.
# Return the "centered" average of an list of ints, which we'll say is the mean
# average of the values, except ignoring the largest and smallest values in the
# list. If there are multiple copies of the smallest value, ignore just one copy,
# and likewise for the largest value. Use int division to produce the final
# average. You may assume that the list is length 3 or more.

ulaz = input("Unesite brojeve (odvojene zarezom): ")
ulaz = ulaz.split(",")

num = []
print(ulaz)

for x in ulaz:
    num.append(int(x))

num.sort()
num = num[1:-1]

def centered_avg(numbers):
    c = 0
    sum = 0
    for x in numbers:
        sum += x
        c += 1
    
    return int(sum / c)

print(centered_avg(num))