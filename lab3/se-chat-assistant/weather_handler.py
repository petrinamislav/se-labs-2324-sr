from query_handler_base import QueryHandlerBase
import requests
import json

class WeatherHandler(QueryHandlerBase):

    def can_process_query(self, query):
        if "weather" in query:
            return True
        return False

    def process(self, query):
        input = query.split()
        city = input[1]

        try:
            res = self.call_api(city)
            txt = res["current"]["temp_c"]
            self.ui.say(txt)
        except:
            self.ui.say("Try again")

        

    def call_api(self, city):
        url = "https://weatherapi-com.p.rapidapi.com/current.json"

        querystring = {"q": city}

        headers = {
            "X-RapidAPI-Key": "2a15944c95mshb85876d85d051f3p18bd33jsn89211b50237f",
            "X-RapidAPI-Host": "weatherapi-com.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers, params=querystring)
        print(response.json())
        return json.loads(response.text)