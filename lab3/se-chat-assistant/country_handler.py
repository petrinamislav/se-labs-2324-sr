from query_handler_base import QueryHandlerBase
import requests
import json

class CountryHandler(QueryHandlerBase):

    def can_process_query(self, query):
        if "country" in query:
            return True
        return False

    def process(self, query):
        input = query.split()
        country = input[1]

        try:
            res = self.call_api(country)
            data = res["data"]
            name = data["name"]
            capital = data["capital"]
            code = data["code"]
            curr = data["currencyCodes"]
            regions = data["numRegions"]
            call = data["callingCode"]
            self.ui.say(f"Country code: {code}")
            self.ui.say(f"Name: {name}")
            self.ui.say(f"Capital: {capital}")
            self.ui.say(f"Currency: {curr}")
            self.ui.say(f"Number of regions: {regions}")
            self.ui.say(f"Calling code: {call}")
        except Exception as e:
            self.ui.say("Try again")

        

    def call_api(self, country):
        url = f"https://wft-geo-db.p.rapidapi.com/v1/geo/countries/{country}"

        headers = {
            "X-RapidAPI-Key": "2a15944c95mshb85876d85d051f3p18bd33jsn89211b50237f",
            "X-RapidAPI-Host": "wft-geo-db.p.rapidapi.com"
        }

        response = requests.get(url, headers=headers)

        return json.loads(response.text)