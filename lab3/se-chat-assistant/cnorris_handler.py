from query_handler_base import QueryHandlerBase
import requests
import json

class ChuckNorrisHandler(QueryHandlerBase):
    def can_process_query(self, query):
        if "cnorris" in query:
            return True
        return False

    def process(self, query):
        try:
            result = self.call_api()
            joke = result["value"]
            self.ui.say(f"{joke}")
        except:
            self.ui.say("Something went wrong")

    def call_api(self):
        url = "https://matchilling-chuck-norris-jokes-v1.p.rapidapi.com/jokes/random"

        headers = {
	        "accept": "application/json",
	        "X-RapidAPI-Key": "2a15944c95mshb85876d85d051f3p18bd33jsn89211b50237f",
	        "X-RapidAPI-Host": "matchilling-chuck-norris-jokes-v1.p.rapidapi.com"
        }

        # response = requests.get(url, headers=headers)
        response = requests.request("GET", url, headers=headers)
        return json.loads(response.text)