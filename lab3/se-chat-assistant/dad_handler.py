from query_handler_base import QueryHandlerBase
import requests
import json

class DadJokeHandler(QueryHandlerBase):

    def can_process_query(self, query):
        if "dadj" in query:
            return True
        return False

    def process (self, query):
        try:
            result = self.call_api()
            txt = result["joke"]
            self.ui.say(f"{txt}")
        except Exception as e:
            self.ui.say("Something went wrong")


    def call_api(self):
        url = "https://daddyjokes.p.rapidapi.com/random"

        headers = {
            "X-RapidAPI-Key": "2a15944c95mshb85876d85d051f3p18bd33jsn89211b50237f",
            "X-RapidAPI-Host": "daddyjokes.p.rapidapi.com"
        }

        response = requests.request("GET", url, headers=headers)
        return json.load(response.text)