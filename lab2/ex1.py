name = input('Enter your name: ')

print(f'Hi {name}')

txt = 'Hi {name}'.format(name = name)
print(txt)

print('Hi %(name)s' %{'name': name})